from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as pltD
import time
import copy
import os
import shutil
import sys
from optimizer import WRSGD, SingleOmegaOptimizer, accumelate_reg_params, MAS_Omega_Vector_Grad_update
from trainer import train_model
from utils import init_store_params, init_params, sanitycheck
from dataset import  ImageFolderTrainVal
from models.mnist import MNIST_Net


def MAS(dataset_path, model_ft, exp_dir, data_dir, reg_sets, reg_lambda=1, 
		norm='L2', num_epochs=100, lr=0.0008, batch_size=200, b1=False):

	"""Call MAS on mainly a sequence of two object recognition tasks with a head for each 
	dataset_path=new dataset path
	exp_dir=where to save the new model
	previous_task_model_path: previous task in the sequence model path to start from 
	reg_sets,data_dir: sets of examples used to compute omega. Here the default is the training set of the last task
	b1=to mimic online importance weight computation, batch size=1
	reg_lambda= regulizer hyper parameter. In object recognition it was set to 1.
	norm=the norm used to compute the gradient of the learned function
	"""
	dsets = torch.load(dataset_path)
	dset_loaders = {x: torch.utils.data.DataLoader(dsets[x], batch_size=150,
											   shuffle=True, num_workers=4) for x in ['train', 'val']}

	dset_sizes = {x: len(dsets[x]) for x in ['train', 'val']}
	dset_classes = dsets['train'].classes
	# model_ft = torch.load(previous_task_model_path)['state_dict']
	use_gpu = torch.cuda.is_available()
	device = torch.device("cuda" if use_gpu else "cpu")
	model_ft.to(device)


	#update omega value
	if b1:
		update_batch_size=1
	else:
		update_batch_size=batch_size

	model_ft=update_weights_params(data_dir, reg_sets, model_ft, update_batch_size, device, norm)
		
	#set the lambda value for the MAS    
	model_ft.reg_params['lambda']=reg_lambda
	
	#get the number of features in this network and add a new task head
	last_layer_index=str(len(model_ft.classifier._modules)-1)

	num_ftrs=model_ft.classifier._modules[last_layer_index].in_features 
	model_ft.classifier._modules[last_layer_index] = nn.Linear(num_ftrs, len(dset_classes))    
   
	#check the computed omega
	sanitycheck(model_ft)
	
	#define the loss function
	criterion = nn.CrossEntropyLoss()
   
	if use_gpu:
		model_ft = model_ft.to(device)
	
	#call the MAS optimizer
	optimizer_ft =WRSGD(model_ft.parameters(), lr, momentum=0.9)
	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)
	
	if not os.path.exists(exp_dir):
		os.makedirs(exp_dir)

	resume=os.path.join(exp_dir,'epoch.pth.tar')
	#train the model
	#this training functin passes the reg params to the optimizer to be used for penalizing changes on important params
	model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, lr, dset_loaders, 
							dset_sizes, device, num_epochs, exp_dir, resume)	
	return model_ft



def MAS_Omega_Accumulation(dataset_path,model_ft,exp_dir,data_dir,reg_sets,reg_lambda=1,norm='L2', num_epochs=100,lr=0.0008,batch_size=200,b1=True):
	"""
	In case of accumelating omega for the different tasks in the sequence, baisically to mimic the setup of standard methods where 
	the reguilizer is computed on the training set. Note that this doesn't consider our adaptation
	dataset_path=new dataset path
	exp_dir=where to save the new model
	previous_task_model_path: previous task in the sequence model path to start from 
	reg_sets: sets of examples used to compute omega. Here the default is the training set of the last task
	b1=to mimic online importance weight computation, batch size=1
	reg_lambda= regulizer hyper parameter. In object recognition it was set to 1.
	norm=the norm used to compute the gradient of the learned function
	"""
	dsets = torch.load(dataset_path)
	dset_loaders = {x: torch.utils.data.DataLoader(dsets[x], batch_size=150,
											   shuffle=True, num_workers=4)
				for x in ['train', 'val']}
	dset_sizes = {x: len(dsets[x]) for x in ['train', 'val']}
	dset_classes = dsets['train'].classes

	use_gpu = torch.cuda.is_available()
	device = torch.device("cuda" if use_gpu else "cpu")
	model_ft.to(device)

	# model_ft = torch.load(previous_task_model_path)
		
	if b1:
		#compute the importance with batch size of 1, to mimic the online setting
		update_batch_size=1
	else:
		update_batch_size=batch_size
		
	#update the omega for the previous task, accumelate it over previous omegas    
	model_ft=update_weights_params(data_dir, reg_sets, model_ft, update_batch_size, device, norm, accumulate=True)
	#set the lambda for the MAS regularizer
	model_ft.reg_params['lambda']=reg_lambda
	
	
	#get the number of features in this network and add a new task head
	last_layer_index=str(len(model_ft.classifier._modules)-1)

	num_ftrs=model_ft.classifier._modules[last_layer_index].in_features 
	model_ft.classifier._modules[last_layer_index] = nn.Linear(num_ftrs, len(dset_classes))  
   
	criterion = nn.CrossEntropyLoss()
	#update the objective based params
	if use_gpu:
		model_ft = model_ft.to(device)
	
	#call the MAS optimizer
	optimizer_ft =WRSGD(model_ft.parameters(), lr, momentum=0.9)
	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)


	if not os.path.exists(exp_dir):
		os.makedirs(exp_dir)

	#if there is a checkpoint to be resumed, in case where the training has stopped before on a given task    
	resume=os.path.join(exp_dir,'epoch.pth.tar')
	
	#train the model
	#this training functin passes the reg params to the optimizer to be used for penalizing changes on important params
	model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, lr, dset_loaders, 
							dset_sizes, device, num_epochs, exp_dir, resume)

							
	
	return model_ft

	
def MAS_sequence(dataset_path,pevious_pathes, model_ft, model_cls, exp_dir,data_dirs,reg_sets,reg_lambda=1,norm='L2', num_epochs=100,lr=0.0008,batch_size=200,weight_decay=1e-5,b1=False,after_freeze=1):
	"""Call MAS on mainly a sequence of tasks with a head for each where between at each step it sees samples from all the previous tasks to approximate the importance weights 
	dataset_path=new dataset path
	exp_dir=where to save the new model
	previous_task_model_path: previous task in the sequence model path to start from 
	pevious_pathes:pathes of previous methods to use the previous heads in the importance weights computation. We assume that each task head is not changed in classification setup of different tasks
	reg_sets,data_dirs: sets of examples used to compute omega. Here the default is the training set of the last task
	b1=to mimic online importance weight computation, batch size=1
	reg_lambda= regulizer hyper parameter. In object recognition it was set to 1.
	norm=the norm used to compute the gradient of the learned function
	"""
	dsets = torch.load(dataset_path)
	dset_loaders = {x: torch.utils.data.DataLoader(dsets[x], batch_size=150,
											   shuffle=True, num_workers=4)
				for x in ['train', 'val']}
	dset_sizes = {x: len(dsets[x]) for x in ['train', 'val']}
	dset_classes = dsets['train'].classes

	use_gpu = torch.cuda.is_available()
	device = torch.device("cuda" if use_gpu else "cpu")   


	# model_ft = torch.load(previous_task_model_path)
	
	if b1:
		update_batch_size=1
	else:
		update_batch_size=batch_size

	model_ft=update_sequence_MAS_weights(data_dirs,reg_sets,pevious_pathes,model_ft,model_cls,update_batch_size,norm)

	model_ft.reg_params['lambda']=reg_lambda
	#model_ft = torchvision.models.alexnet()
	last_layer_index=str(len(model_ft.classifier._modules)-1)

	num_ftrs=model_ft.classifier._modules[last_layer_index].in_features 
	model_ft.classifier._modules[last_layer_index] = nn.Linear(num_ftrs, len(dset_classes))  
   
	#check the values of omega
	sanitycheck(model_ft)
	criterion = nn.CrossEntropyLoss()
	#update the objective based params
	
	if use_gpu:
		model_ft = model_ft.to(device)
	

	#our optimizer
	optimizer_ft =WRSGD(model_ft.parameters(), lr, momentum=0.9,weight_decay=weight_decay)
	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)
	#exp_dir='/esat/monkey/raljundi/pytorch/CUB11f_hebbian_finetuned'
	if not os.path.exists(exp_dir):
		os.makedirs(exp_dir)

	resume=os.path.join(exp_dir,'epoch.pth.tar')

	model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, lr, dset_loaders, 
							dset_sizes, device, num_epochs, exp_dir, resume)	
	return model_ft




def update_weights_params(data_dir, reg_sets, model_ft, batch_size, device, norm='L2', accumulate=False):
	"""update the importance weights based on the samples included in the reg_set. Assume starting from zero omega
	
	   model_ft: the model trained on the previous task 
	"""
	data_transform =  transforms.Compose([
		transforms.Scale(256),
		transforms.CenterCrop(224),
		transforms.ToTensor(),
		transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
		
	#prepare the dataset
	dset_loaders=[]
	for data_path in reg_sets:
	
		# if so then the reg_sets is a dataset by its own, this is the case for the mnist dataset
		if data_dir is not None:
			dset = ImageFolderTrainVal(data_dir, data_path, data_transform)
		else:
			dset=torch.load(data_path)
			dset=dset['train']
		dset_loader= torch.utils.data.DataLoader(dset, batch_size=batch_size,
											   shuffle=False, num_workers=4)
		dset_loaders.append(dset_loader)

	

	if accumulate:
		#store the previous omega, set values to zero
		reg_params=init_store_params(model_ft)
		model_ft.reg_params=reg_params
	else:
		#inialize the importance params,omega, to zero
		reg_params=init_params(model_ft)
		model_ft.reg_params=reg_params

	#define the importance weight optimizer. Actually it is only one step. It can be integrated at the end of the first task training
	optimizer_ft = SingleOmegaOptimizer(model_ft.parameters(), lr=0.0001, momentum=0.9)
	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)
	
	if norm=='L2':
		print('********************MAS with L2 norm***************')
		#compute the imporance params
		model_ft = compute_importance_l2(model_ft, optimizer_ft, exp_lr_scheduler, dset_loaders, device)
	else:
		if norm=='vector':
			optimizer_ft=MAS_Omega_Vector_Grad_update(model_ft.parameters(), lr=0.0001, momentum=0.9)
			model_ft = compute_importance_gradient_vector(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders,use_gpu)

		else:
			model_ft = compute_importance(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders, device)

	if accumulate:
		#accumelate the new importance params  with the prviously stored ones (previous omega)
		reg_params=accumelate_reg_params(model_ft)
		model_ft.reg_params=reg_params


	return model_ft


# def accumulate_MAS_weights(data_dir, reg_sets, model_ft, batch_size, norm='L2'):
# 	"""accumelate the importance params: stores the previously computed omega, compute omega on the last previous task
# 			and accumelate omega resulting on  importance params for all the previous tasks
# 	   reg_sets:either a list of files containing the samples used for computing the importance param like train or train and test
# 				or pytorch dataset, then train set is used
# 	   data_dir:
# 	"""
# 	data_transform =  transforms.Compose([
# 		transforms.Scale(256),
# 		transforms.CenterCrop(224),
# 		transforms.ToTensor(),
# 		transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
		
	

# 	#prepare the dataset
# 	dset_loaders=[]
# 	for data_path in reg_sets:
	
# 		# if data_dir is not None then the reg_sets is a dataset by its own, this is the case for the mnist dataset
# 		if data_dir is not None:
# 			dset = ImageFolderTrainVal(data_dir, data_path, data_transform)
# 		else:
# 			dset=torch.load(data_path)
# 			dset=dset['train']
# 		dset_loader= torch.utils.data.DataLoader(dset, batch_size=batch_size,
# 											   shuffle=False, num_workers=4)
# 		dset_loaders.append(dset_loader)
# 	#=============================================================================
	
# 	use_gpu = torch.cuda.is_available()

# 	#store the previous omega, set values to zero
# 	reg_params=init_store_params(model_ft)
# 	model_ft.reg_params=reg_params
# 	#define the importance weight optimizer. Actually it is only one step. It can be integrated at the end of the first task training
# 	optimizer_ft = SingleOmegaOptimizer(model_ft.parameters(), lr=0.0001, momentum=0.9)
# 	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)

# 	if norm=='L2':
# 		print('********************objective with L2 norm***************')
# 		model_ft =compute_importance_l2(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders, use_gpu)
# 	else:
# 		model_ft =compute_importance(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders, use_gpu)

# 	#accumelate the new importance params  with the prviously stored ones (previous omega)
# 	reg_params=accumelate_reg_params(model_ft)
# 	model_ft.reg_params=reg_params
# 	sanitycheck(model_ft)   
# 	return model_ft





def update_sequence_MAS_weights(data_dirs, reg_sets, previous_models, model_ft, model_cls, batch_size, norm='L2'):
	"""updates a task in a sequence while computing omega from scratch each time on the previous tasks
	   previous_models: to use their heads for compute the importance params
	"""
	data_transform =  transforms.Compose([
		transforms.Scale(256),
		transforms.CenterCrop(224),
		transforms.ToTensor(),
		transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
	t=0    
	last_layer_index=str(len(model_ft.classifier._modules)-1)
	for num_classes, model_path in previous_models:
		pre_model =  model_cls(num_classes=2)
		pre_model.load_state_dict(torch.load(model_path)['state_dict'])

		#get previous task head
		model_ft.classifier._modules[last_layer_index] = pre_model.classifier._modules[last_layer_index]

		# if data_dirs is None then the reg_sets is a dataset by its own, this is the case for the mnist dataset
		if data_dirs is not None:
			dset = ImageFolderTrainVal(data_dirs[t], reg_sets[t], data_transform)
		else:
			dset=torch.load(reg_sets[t])
			dset=dset['train']

		dset_loader= torch.utils.data.DataLoader(dset, batch_size=batch_size,
											   shuffle=False, num_workers=4)
	#=============================================================================
		use_gpu = torch.cuda.is_available()
		device = torch.device("cuda" if use_gpu else "cpu")   
		model_ft.to(device)
	   
		if t==0:
			#initialize to zero
			reg_params=init_params(model_ft)
		else:
			#store previous task param
			reg_params=init_store_params(model_ft)

		model_ft.reg_params=reg_params
		#define the importance weight optimizer. Actually it is only one step. It can be integrated at the end of the first task training
		optimizer_ft = SingleOmegaOptimizer(model_ft.parameters(), lr=0.0001, momentum=0.9)
		exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)
		#legacy code
		dset_loaders=[dset_loader]
		#compute param importance
		if norm=='L2':
			
			print('********************objective with L2 norm***************')
			model_ft = compute_importance_l2(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders, device)
		else:
			model_ft = compute_importance(model_ft, optimizer_ft,exp_lr_scheduler, dset_loaders, device)
		if t>0:
			reg_params=accumelate_reg_params(model_ft)
		
		model_ft.reg_params=reg_params
		t=t+1
	sanitycheck(model_ft)   
	return model_ft




def compute_importance_l2(model, optimizer, lr_scheduler, dset_loaders, device):
	"""Mimic the depoloyment setup where the model is applied on some samples and those are used to update the importance params
	   Uses the L2norm of the function output. This is what we MAS uses as default
	"""
	print('dictoinary length'+str(len(dset_loaders)))
	#reg_params=model.reg_params
	since = time.time()

	best_model = model
	best_acc = 0.0
	
	epoch=1
	#it does nothing here
	lr_scheduler.step()
	# optimizer = lr_scheduler(optimizer, epoch,1)
	model.eval()  # Set model to training mode so we get the gradient


	running_loss = 0.0
	running_corrects = 0
   
	# Iterate over data.
	index=0
	for dset_loader in dset_loaders:
		for data in dset_loader:
			# get the inputs
			inputs, labels = data
			inputs, labels = inputs.to(device), labels.to(device)

			if inputs.size(1)==1 and len(inputs.size())==3:
				
				#for mnist, there is no channel 
				#and  to avoid problems we remove that additional dimension generated by pytorch transformation
				inputs=inputs.view(inputs.size(0),inputs.size(2))            

			# zero the parameter gradients
			optimizer.zero_grad()
			
			# forward
			outputs = model(inputs)
			_, preds = torch.max(outputs.data, 1)

			#compute the L2 norm of output 
			Target_zeros=torch.zeros(outputs.size())
			Target_zeros=Target_zeros.cuda()
			Target_zeros=Variable(Target_zeros)
			#note no avereging is happening here
			loss = torch.nn.MSELoss(size_average=False)

			targets = loss(outputs,Target_zeros) 
			#compute the gradients
			targets.backward()

			#update the parameters importance
			optimizer.step(model.reg_params, index, labels.size(0))

			print('batch number ',index)
			#nessecary index to keep the running average
			index+=1
   
	return model


def compute_importance(model, optimizer, lr_scheduler,dset_loaders, device):
	"""Mimic the depoloyment setup where the model is applied on some samples and those are used to update the importance params
	   Uses the L1norm of the function output
	"""
	print('dictoinary length'+str(len(dset_loaders)))
   
	since = time.time()

	best_model = model
	best_acc = 0.0
	
	#pdb.set_trace()
			
	epoch=1
	#it does nothing here, can be removed
	lr_scheduler.step()
	# optimizer = lr_scheduler(optimizer, epoch,1)
	model.eval()  # Set model to training mode so we get the gradient


	running_loss = 0.0
	running_corrects = 0
   
	# Iterate over data.
	index=0
	for dset_loader in dset_loaders:
		#pdb.set_trace()
		for data in dset_loader:
			# get the inputs
			inputs, labels = data
			inputs, labels = inputs.to(device), labels.to(device)

			# zero the parameters gradients
			optimizer.zero_grad()

			# forward
			outputs = model(inputs)
			_, preds = torch.max(outputs.data, 1)
	  

		   #compute the L1 norm of the function output
			Target_zeros=torch.zeros(outputs.size())
			Target_zeros=Target_zeros.cuda()
			Target_zeros=Variable(Target_zeros,requires_grad=False)
	   
			loss = torch.nn.L1Loss(size_average=False)

			targets = loss(outputs,Target_zeros) 
			#compute gradients
			targets.backward()
		
			print('batch number ',index)
			#update parameters importance
			optimizer.step(model.reg_params,index,labels.size(0))
			#nessecary index to keep the running average
			index+=1
   
	return model


def compute_importance_gradient_vector(model, optimizer, lr_scheduler, dset_loaders, device):
	"""Mimic the depoloyment setup where the model is applied on some samples and those are used to update the importance params
	   Uses the gradient of the function output
	"""
	print('dictoinary length'+str(len(dset_loaders)))
	#reg_params=model.reg_params
	since = time.time()

	best_model = model
	best_acc = 0.0
	
	
	
		
	epoch=1
	optimizer = lr_scheduler(optimizer, epoch,1)
	model.eval()  # Set model to training mode so we get the gradient


	running_loss = 0.0
	running_corrects = 0
   
	# Iterate over data.
	index=0
	for dset_loader in dset_loaders:
		for data in dset_loader:
			# get the inputs
			inputs, labels = data

			# wrap them in Variable
			inputs, labels = data
			inputs, labels = inputs.to(device), labels.to(device)

			# zero the parameter gradients
			optimizer.zero_grad()
			
			# forward
			outputs = model(inputs)
			_, preds = torch.max(outputs.data, 1)
	 
		   
			for output_i in range(0,outputs.size(1)):
				Target_zeros=torch.zeros(outputs.size())
				Target_zeros=Target_zeros.cuda()
				Target_zeros[:,output_i]=1
				Target_zeros=Variable(Target_zeros,requires_grad=False)
				targets=torch.sum(outputs*Target_zeros)
				if output_i==(outputs.size(1)-1):
					targets.backward()
				else:
					targets.backward(retain_graph=True )
					
				optimizer.step(model.reg_params,True,index,labels.size(0))
				optimizer.zero_grad()
			
		#print('step')
			optimizer.step(model.reg_params,False,index,labels.size(0))
			print('batch number ',index)
			index+=1
   
	return model




# def exp_lr_scheduler(optimizer, epoch, init_lr=0.0008, lr_decay_epoch=45):
# 	"""Decay learning rate by a factor of 0.1 every lr_decay_epoch epochs."""
# 	lr = init_lr * (0.1**(epoch // lr_decay_epoch))
# 	print('lr is '+str(lr))
# 	if epoch % lr_decay_epoch == 0:
# 		print('LR is set to {}'.format(lr))

# 	for param_group in optimizer.param_groups:
# 		param_group['lr'] = lr

# 	return optimizer



def fine_tune_SGD(model, dataset_path, exp_dir, batch_size=100, 
				num_epochs=100, lr=0.0004, init_freeze=1):
   
	print('lr is ' + str(lr))
	
	dsets = torch.load(dataset_path)
	dset_loaders = dict()
	dset_sizes = dict()

	for x in ['train', 'val']:
		dset_loaders[x] = torch.utils.data.DataLoader(dsets[x], batch_size=batch_size,
						shuffle=True, num_workers=4) 
		dset_sizes[x] =  len(dsets[x])

	dset_classes = dsets['train'].classes
	use_gpu = torch.cuda.is_available()
	device = torch.device("cuda" if use_gpu else "cpu")

	# resume=os.path.join(exp_dir,'epoch.pth.tar')
	# if os.path.isfile(resume):
	# 		model_ft = MNIST_Net()
	# 		checkpoint = torch.load(resume)
	# 		model_ft = checkpoint['model']

	# if not os.path.isfile(model_path):
	# 	model_ft = models.alexnet(pretrained=True)
	   
	# else:
	# 	model_ft=torch.load(model_path)


	if not init_freeze:    
		num_ftrs = model.classifier[6].in_features 
		model.classifier._modules['6'] = nn.Linear(num_ftrs, len(dset_classes))  

	if not os.path.exists(exp_dir):
		os.makedirs(exp_dir)

	if use_gpu:
		model = model.cuda()
	

	criterion = nn.CrossEntropyLoss()
	optimizer_ft =  optim.SGD(model.parameters(), lr, momentum=0.9)
	exp_lr_scheduler = optim.lr_scheduler.StepLR(optimizer_ft, step_size=54, gamma=0.1)
	model = train_model(model, criterion, optimizer_ft, exp_lr_scheduler, lr, 
							dset_loaders, dset_sizes, device, num_epochs, exp_dir, '', mas=False)
	
	return model



