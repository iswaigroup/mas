
import torch
import torch.nn as nn
import pdb
from torch.autograd import Variable
import torch.nn.functional as F


class MNIST_Net(nn.Module):
	def __init__(self,num_classes=10,hidden_size=256):
		super(MNIST_Net, self).__init__()

		self.classifier = nn.Sequential(
		   
			nn.Linear( 28 * 28, hidden_size),
			nn.ReLU(inplace=True),
			nn.Linear(hidden_size, hidden_size),
			nn.ReLU(inplace=True),
			nn.Linear(hidden_size, num_classes))

	def forward(self, x):
		x = self.classifier(x)
		return x

	def _initialize_weights(self):
		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
				if m.bias is not None:
					nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.BatchNorm2d):
				nn.init.constant_(m.weight, 1)
				nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.Linear):
				nn.init.normal_(m.weight, 0, 0.01)
				nn.init.constant_(m.bias, 0)

				


class MNIST_Net_buttelNeck(nn.Module):
	def __init__(self,num_classes=10,hidden_size=256,buttel_neck=10):
		super(MNIST_Net_buttelNeck, self).__init__()

		self.classifier = nn.Sequential(
		   
			nn.Linear( 28 * 28, hidden_size),
			nn.ReLU(inplace=True),
			nn.Linear(hidden_size, hidden_size),
			nn.ReLU(inplace=True),
			nn.Linear(hidden_size, buttel_neck),
			nn.ReLU(inplace=True),
			nn.Linear(buttel_neck, num_classes))

	def forward(self, x):
		x = self.classifier(x)
		return x


class Net(nn.Module):
	def __init__(self, num_classes=10):
		super(Net, self).__init__()
		# self.conv1 = nn.Conv2d(1, 32, 3, 1)
		# self.conv2 = nn.Conv2d(32, 64, 3, 1)
		# self.dropout1 = nn.Dropout2d(0.25)
		# self.dropout2 = nn.Dropout2d(0.5)
		# self.fc1 = nn.Linear(9216, 128)
		# self.classifier = nn.Linear(128, num_classes)

		self.reg_params = dict()

		self.features = nn.Sequential(
			nn.Conv2d(1, 32, 3, 1),
			nn.ReLU(inplace=True),
			nn.Conv2d(32, 64, 3, 1),
			nn.ReLU(inplace=True),
			nn.MaxPool2d(kernel_size=2, stride=2),
			nn.Dropout2d(0.25)
		)

		self.classifier = nn.Sequential(
			nn.Linear(9216, 128),
			nn.ReLU(inplace=True),
			nn.Dropout2d(0.5),
			nn.Linear(128, num_classes),
		)

	def forward(self, x):
		x = self.features(x)
		x = torch.flatten(x, 1)
		output = self.classifier(x)
		# x = self.conv1(x)
		# x = F.relu(x)
		# x = self.conv2(x)
		# x = F.relu(x)
		# x = F.max_pool2d(x, 2)
		# x = self.dropout1(x)
		# x = torch.flatten(x, 1)
		# x = self.fc1(x)
		# x = F.relu(x)
		# x = self.dropout2(x)
		# x = self.classifier(x)
		# output = F.log_softmax(x, dim=1)
		return output

	def _initialize_weights(self):
		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
				if m.bias is not None:
					nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.BatchNorm2d):
				nn.init.constant_(m.weight, 1)
				nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.Linear):
				nn.init.normal_(m.weight, 0, 0.01)
				nn.init.constant_(m.bias, 0)

