from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
# import matplotlib.pyplot as plt
import torch.optim as optim
import time
import copy
import os
import math
import shutil
from torch.utils.data import DataLoader
from data.mnist import MNIST_Split



class WRSGD(optim.SGD):
	r"""Implements SGD training with importance params regulization. IT inherents stochastic gradient descent (optionally with momentum).
	Nesterov momentum is based on the formula from
	
	"""

	def __init__(self, params, lr=0.001, momentum=0, dampening=0,
				 weight_decay=0, nesterov=False):
		
		super(WRSGD, self).__init__(params, lr, momentum, dampening, weight_decay, nesterov)

	def __setstate__(self, state):
		super(WRSGD, self).__setstate__(state)
	   
		
	def step(self, reg_params, closure=None):
		"""Performs a single optimization step.
		Arguments:
			reg_params: omega of all the params
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
	   

		loss = None
		if closure is not None:
			loss = closure()
		
		reg_lambda=reg_params.get('lambda')
	   
		for group in self.param_groups:
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
		   
			for p in group['params']:
				if p.grad is None:
					continue
				d_p = p.grad.data
			   
				#MAS PART CODE GOES HERE
				#if this param has an omega to use for regulization
				if p in reg_params:
					
					reg_param=reg_params.get(p)
					#get omega for this parameter
					omega=reg_param.get('omega')
					#initial value when the training start
					init_val=reg_param.get('init_val')
					
					curr_wegiht_val=p.data
					#move the tensors to cuda
					init_val=init_val.cuda()
					omega=omega.cuda()
					
					#get the difference
					weight_dif=curr_wegiht_val.add(-1,init_val)
					#compute the MAS penalty
					regulizer=weight_dif.mul(2*reg_lambda*omega)
					del weight_dif
					del curr_wegiht_val
					del omega
					del init_val
					#add the MAS regulizer to the gradient
					d_p.add_(regulizer)
					del regulizer
				#MAS PARAT CODE ENDS
				if weight_decay != 0:
				   
					d_p.add_(weight_decay,p.data.sign())
				   
 
				if momentum != 0:
					param_state = self.state[p]
					if 'momentum_buffer' not in param_state:
						buf = param_state['momentum_buffer'] = d_p.clone()
					else:
						buf = param_state['momentum_buffer']
						buf.mul_(momentum).add_(1 - dampening, d_p)
					if nesterov:
						d_p = d_p.add(momentum, buf)
					else:
						d_p = buf
				
				p.data.add_(-group['lr'], d_p)
				
		return loss #ELASTIC SGD



class SingleOmegaOptimizer(optim.SGD):
	"""
	Update the paramerter importance using the gradient of the function output norm. To be used at deployment time.
	reg_params:parameters omega to be updated
	batch_index,batch_size:used to keep a running average over the seen samples
	"""

	def __init__(self, params, lr=0.001, momentum=0, dampening=0,
				 weight_decay=0, nesterov=False):
		
		super(SingleOmegaOptimizer, self).__init__(params, lr,momentum,dampening,weight_decay,nesterov)
		
	def __setstate__(self, state):
		super(SingleOmegaOptimizer, self).__setstate__(state)
	   

	def step(self, reg_params, batch_index, batch_size, closure=None):
		"""
		Performs a single parameters importance update setp
		"""

		#print('************************DOING A STEP************************')
 
		loss = None
		if closure is not None:
			loss = closure()
			 
		for group in self.param_groups:
   
			#if the parameter has an omega to be updated
			for p in group['params']:
		  
				#print('************************ONE PARAM************************')
				
				if p.grad is None:
					continue
			   
				if p in reg_params:
					d_p = p.grad.data
				  
					
					#HERE MAS IMPOERANCE UPDATE GOES
					#get the gradient
					unreg_dp = p.grad.data.clone()
					reg_param=reg_params.get(p)
					
					zero=torch.FloatTensor(p.data.size()).zero_()
					#get parameter omega
					omega=reg_param.get('omega')
					omega=omega.cuda()
	
					
					#sum up the magnitude of the gradient
					prev_size=batch_index*batch_size
					curr_size=(batch_index+1)*batch_size
					omega=omega.mul(prev_size)
					
					omega=omega.add(unreg_dp.abs_())
					#update omega value
					omega=omega.div(curr_size)
					if omega.equal(zero.cuda()):
						print('omega after zero')

					reg_param['omega']=omega
				   
					reg_params[p]=reg_param
					#HERE MAS IMPOERANCE UPDATE ENDS
		return loss#HAS NOTHING TO DO

  
class MAS_Omega_Vector_Grad_update(optim.SGD):
	"""
	Update the paramerter importance using the gradient of the function output. To be used at deployment time.
	reg_params:parameters omega to be updated
	batch_index,batch_size:used to keep a running average over the seen samples
	"""

	def __init__(self, params, lr=0.001, momentum=0, dampening=0,
				 weight_decay=0, nesterov=False):
		
		super(MAS_Omega_Vector_Grad_update, self).__init__(params, lr,momentum,dampening,weight_decay,nesterov)
		
	def __setstate__(self, state):
		super(MAS_Omega_Vector_Grad_update, self).__setstate__(state)
	   

	def step(self, reg_params,intermediate,batch_index,batch_size,closure=None):
		"""
		Performs a single parameters importance update setp
		"""

		#print('************************DOING A STEP************************')

		loss = None
		if closure is not None:
			loss = closure()
		index=0
	 
		for group in self.param_groups:
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
	
			for p in group['params']:
		  
				#print('************************ONE PARAM************************')
				
				if p.grad is None:
					continue
				
				if p in reg_params:
					d_p = p.grad.data
					unreg_dp = p.grad.data.clone()
					#HERE MAS CODE GOES
					reg_param=reg_params.get(p)
					
					zero=torch.FloatTensor(p.data.size()).zero_()
					omega=reg_param.get('omega')
					omega=omega.cuda()
	
					
					#get the magnitude of the gradient
					if intermediate:
						if 'w' in reg_param.keys():
							w=reg_param.get('w')
						else:
							w=torch.FloatTensor(p.data.size()).zero_()
						w=w.cuda()
						w=w.add(unreg_dp.abs_())
						reg_param['w']=w
					else:
					   
					   #sum the magnitude of the gradients
						w=reg_param.get('w')
						prev_size=batch_index*batch_size
						curr_size=(batch_index+1)*batch_size
						omega=omega.mul(prev_size)
						omega=omega.add(w)
						omega=omega.div(curr_size)
						reg_param['w']=zero.cuda()
						
						if omega.equal(zero.cuda()):
							print('omega after zero')

					reg_param['omega']=omega
					#pdb.set_trace()
					reg_params[p]=reg_param
				index+=1
		return loss
#importance_dictionary: contains all the information needed for computing the w and omega
  

  



def accumelate_reg_params(model,freeze_layers=[]):
	"""accumelate the newly computed omega with the previously stroed one from the old previous tasks"""
	reg_params=model.reg_params
	for name, param in model.named_parameters():
		if not name in freeze_layers:
			if param in reg_params:
				reg_param=reg_params.get(param)
				print('restoring previous omega',name)
				prev_omega=reg_param.get('prev_omega')
				prev_omega=prev_omega.cuda()
				
				new_omega=(reg_param.get('omega')).cuda()
				acc_omega=torch.add(prev_omega,new_omega)
				
				del reg_param['prev_omega']
				reg_param['omega'] = acc_omega
			   
				reg_params[param]=reg_param
				del acc_omega
				del new_omega
				del prev_omega
		else:
			if param in reg_params: 
				reg_param=reg_params.get(param)
				print('removing unused omega',name)
				del reg_param['omega'] 
				del reg_params[param]             
	return reg_params






class WRAdam(optim.Adam):
    r"""Implements Adam algorithm.

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_
            (default: False)

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, amsgrad=False):
		super(WRAdam, self).__init__(params, lr, betas, eps, weight_decay, amsgrad)

	def __setstate__(self, state):
		super(WRAdam, self).__setstate__(state)
	   

    @torch.no_grad()
    def step(self, reg_params, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                grad = p.grad

				#MAS PART CODE GOES HERE
				#if this param has an omega to use for regulization
				if p in reg_params:
					
					reg_param=reg_params.get(p)
					#get omega for this parameter
					omega=reg_param.get('omega')
					#initial value when the training start
					init_val=reg_param.get('init_val')
					
					curr_wegiht_val=p.data
					#move the tensors to cuda
					init_val=init_val.cuda()
					omega=omega.cuda()
					
					#get the difference
					weight_dif=curr_wegiht_val.add(-1,init_val)
					#compute the MAS penalty
					regulizer=weight_dif.mul(2*reg_lambda*omega)
					del weight_dif
					del curr_wegiht_val
					del omega
					del init_val
					#add the MAS regulizer to the gradient
					d_p.add_(regulizer)
					del regulizer
				#MAS PARAT CODE ENDS


                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
                amsgrad = group['amsgrad']

                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p, memory_format=torch.preserve_format)
                    # Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p, memory_format=torch.preserve_format)
                    if amsgrad:
                        # Maintains max of all exp. moving avg. of sq. grad. values
                        state['max_exp_avg_sq'] = torch.zeros_like(p, memory_format=torch.preserve_format)

                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                if amsgrad:
                    max_exp_avg_sq = state['max_exp_avg_sq']
                beta1, beta2 = group['betas']

                state['step'] += 1
                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']

                if group['weight_decay'] != 0:
                    grad = grad.add(p, alpha=group['weight_decay'])

                # Decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(grad, alpha=1 - beta1)
                exp_avg_sq.mul_(beta2).addcmul_(grad, grad, value=1 - beta2)
                if amsgrad:
                    # Maintains the maximum of all 2nd moment running avg. till now
                    torch.max(max_exp_avg_sq, exp_avg_sq, out=max_exp_avg_sq)
                    # Use the max. for normalizing running avg. of gradient
                    denom = (max_exp_avg_sq.sqrt() / math.sqrt(bias_correction2)).add_(group['eps'])
                else:
                    denom = (exp_avg_sq.sqrt() / math.sqrt(bias_correction2)).add_(group['eps'])

                step_size = group['lr'] / bias_correction1

                p.addcdiv_(exp_avg, denom, value=-step_size)

        return loss