from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
# import matplotlib.pyplot as plt
import torch.optim as optim
import time
import copy
import os
import math
import shutil
from torch.utils.data import DataLoader


def init_params(model,freeze_layers=[]):
    """initialize an omega for each parameter to zero"""
    reg_params={}
    for name, param in model.named_parameters():
        if not name in freeze_layers:
            print('initializing param',name)
            omega=torch.FloatTensor(param.size()).zero_()
            init_val=param.data.clone()
            reg_param={}
            reg_param['omega'] = omega
            #initialize the initial value to that before starting training
            reg_param['init_val'] = init_val
            reg_params[param]=reg_param
    return reg_params
   


def init_store_params(model,freeze_layers=[]):
    """set omega to zero but after storing its value in a temp omega in which later we can accumolate them both"""
    reg_params=model.reg_params
    for name, param in model.named_parameters():
        #in case there some layers that are not trained
        if not name in freeze_layers:
            if param in reg_params:
                reg_param=reg_params.get(param)
                print('storing previous omega',name)
                prev_omega=reg_param.get('omega')
                new_omega=torch.FloatTensor(param.size()).zero_()
                init_val=param.data.clone()
                reg_param['prev_omega']=prev_omega   
                reg_param['omega'] = new_omega
                
                #initialize the initial value to that before starting training
                reg_param['init_val'] = init_val
                reg_params[param]=reg_param
                
        else:
            if param in reg_params: 
                reg_param=reg_params.get(param)
                print('removing unused omega',name)
                del reg_param['omega'] 
                del reg_params[param]
    return reg_params
   



    
def save_checkpoint(state, filename='checkpoint.pth.tar'):
    #best_model = copy.deepcopy(model)
    torch.save(state, filename)
   

def sanitycheck(model):
    for name, param in model.named_parameters():
           
            print (name)
            if param in model.reg_params:
            
                reg_param=model.reg_params.get(param)
                omega=reg_param.get('omega')
                
                print('omega max is',omega.max())
                print('omega min is',omega.min())
                print('omega mean is',omega.mean())

